package com.lshershun.weatherapp.ui.map

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

import com.lshershun.weatherapp.R
import com.lshershun.weatherapp.entities.Place
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class MapsFragment : Fragment(), OnMapReadyCallback {

    private val vm: MapsViewModel by sharedViewModel()
    private lateinit var mMap: GoogleMap
    private lateinit var root: View
    private lateinit var mapView: MapView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        root = inflater.inflate(R.layout.maps_fragment, container, false)
        mapView = root.findViewById(R.id.map)
        root.findViewById<View>(R.id.clean)
            .setOnClickListener { vm.cleanPlaces() }
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
        vm.onStart()
        return root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        vm.messageEvent.observe(this, Observer {
            it?.apply {
                if (action != null && actionMessage != null) {
                    Snackbar.make(root, message, Snackbar.LENGTH_LONG)
                        .addCallback(object : Snackbar.Callback(){
                            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                                negativeAction?.let {
                                    negativeAction!!.invoke()
                                }
                            }
                        })
                        .setAction(actionMessage!!) { action!!.invoke()}
                        .show()
                } else {
                    Snackbar.make(root, message, Snackbar.LENGTH_SHORT).show()
                }

            }
        })
    }



    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapClickListener { vm.onPositionSelected(it.latitude, it.longitude) }
        vm.selectedPlace.observe(this, Observer {
            if (it != null) addMarker(it)
            else cleanMarkers()
        })
        vm.highlightEvent.observe(this, Observer {
            mMap.animateCamera(
                CameraUpdateFactory.newCameraPosition(
                    CameraPosition.Builder()
                        .target(LatLng(it.lat, it.lon))
                        .build()
                )
            )
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.map, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.action_ok){
            vm.actionOk(Runnable { findNavController().navigateUp() })
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun cleanMarkers() {
        mMap.clear()
    }

    private fun addMarker(it: Place) {
        mMap.clear()
        mMap.addMarker(MarkerOptions().position(LatLng(it.lat, it.lon)))
    }

    override fun onResume() {
        mapView.onResume()
        super.onResume()
    }


    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }


}
