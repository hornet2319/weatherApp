package com.lshershun.weatherapp.ui.details


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.lshershun.weatherapp.R
import com.lshershun.weatherapp.entities.Forecast
import com.lshershun.weatherapp.ui.map.MapsViewModel
import kotlinx.android.synthetic.main.forecast_fragment.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ForecastFragment : Fragment() {

    private val vm: ForecastViewModel by viewModel()
    private val mapVM: MapsViewModel by sharedViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.forecast_fragment, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)

        vm.forecast.observe(this, Observer<Forecast> {
            it.apply {
                header.text = "Nearest location: $city_name, ($country_code)  GPS:$lat:$lon\n"
                forecastText.text = "Temperature $temp deg, feels like $app_temp deg," +
                        "\nwind speed $wind_spd m/s, Relative humidity $rh%, Cloud coverage $clouds%,\n" +
                        "Visibility $vis km, Estimated Solar Radiation $solar_rad W/m^2,\nSolar elevation angle $elev_angle deg"
            }

//            forecastText.text = it.toString() //fixme test
        })

        vm.errorEvent.observe(this, Observer {error ->
            view?.let {
                val snackbar = Snackbar.make(it, error.messageRes, Snackbar.LENGTH_INDEFINITE)
                snackbar
                    .setAction(R.string.ok) {
                        if (error.isCritical){
                            activity?.finish()
                        } else snackbar.dismiss()
                    }
                snackbar.show()
            }

        })
        vm.navigationEvent.observe(this, Observer { findNavController().navigate(it) })
        mapVM.selectedPlace.observe(this, Observer {
            vm.updateForecast()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.detail, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_map -> {
                findNavController().navigate(R.id.mapsFragment2); return true
            }
            R.id.action_refresh -> {
                vm.updateForecast(); return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
