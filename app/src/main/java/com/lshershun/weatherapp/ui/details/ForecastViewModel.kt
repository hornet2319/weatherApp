package com.lshershun.weatherapp.ui.details

import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lshershun.weatherapp.BuildConfig
import com.lshershun.weatherapp.R
import com.lshershun.weatherapp.entities.Forecast
import com.lshershun.weatherapp.repository.Repository
import com.lshershun.weatherapp.tools.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable

data class Error(@StringRes val messageRes: Int, val isCritical:Boolean = false)

class ForecastViewModel(private val repository: Repository) : ViewModel() {
    val forecast = MutableLiveData<Forecast>()
    val navigationEvent = SingleLiveEvent<Int>()
    val errorEvent = SingleLiveEvent<Error>()
    private val compositeDisposable = CompositeDisposable()

    init {
        getForecasts()
    }

    private fun getForecasts() {
        if (!repository.isInternetConnected()) {
            errorEvent.postValue(Error(R.string.connection_error_message))
        }
        compositeDisposable.add( repository
            .getLastCity()
            .doOnError {
                if (!repository.isInternetConnected()) {
                    Log.d(BuildConfig.TAG, "navigating to map fragment")
                    errorEvent.postValue(Error(R.string.data_main_error, true))
                } else {
                    Log.d(BuildConfig.TAG, "navigating to map fragment")
                    //navigating to maps screen, cuz we have no saved places
                    navigationEvent.postValue(R.id.mapsFragment2)
                }
            }
            .toFlowable()
            .flatMap { repository.getForecasts(it) }
            .subscribe({
                Log.d(BuildConfig.TAG, "response\n$it")
                forecast.postValue(it)
            }, { it.printStackTrace() })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun updateForecast() {
        Log.d(BuildConfig.TAG, "updating forecast")
        compositeDisposable.clear()
        getForecasts()
    }
}
