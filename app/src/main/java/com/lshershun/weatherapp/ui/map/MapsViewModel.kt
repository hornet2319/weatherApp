package com.lshershun.weatherapp.ui.map

import android.annotation.SuppressLint
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lshershun.weatherapp.R
import com.lshershun.weatherapp.entities.Place
import com.lshershun.weatherapp.repository.Repository
import com.lshershun.weatherapp.tools.SingleLiveEvent

data class MessageEvent(
    @StringRes var message: Int,
    @StringRes var actionMessage: Int? = null,
    var action: (()->Unit)? = null,
    var negativeAction: (()->Unit)? = null
)

class MapsViewModel(private val repository: Repository) : ViewModel() {
    private var cachedPlace: Place? = null
    val selectedPlace = MutableLiveData<Place>()
    val messageEvent = SingleLiveEvent<MessageEvent>()
    val highlightEvent = SingleLiveEvent<Place>()


    fun cleanPlaces() {
        cachedPlace = selectedPlace.value
        selectedPlace.postValue(null)
        cachedPlace?.let {
            messageEvent.postValue(
                MessageEvent(R.string.place_cleared, R.string.undo,  {
                    restoreCachedPlace()
                }) {
                    repository.cleanPlaces().subscribe()
                }
            )
        }
    }

    private fun restoreCachedPlace() {
        cachedPlace?.let {
            selectedPlace.postValue(it)
        }

    }

    fun onPositionSelected(lat: Double, lon: Double) {
        val place = Place(lat, lon)
        selectedPlace.postValue(place)
        messageEvent.postValue(
            MessageEvent(R.string.place_change_success, R.string.show, {
                highlightEvent.postValue(place)
            })
        )
    }

    @SuppressLint("CheckResult")
    fun actionOk(successFunction: Runnable) {
        if (selectedPlace.value == null) {
            messageEvent.postValue(
                MessageEvent(R.string.please_select_place)
            )
        }
        selectedPlace.value?.let {
            repository.savePlace(it)
                .subscribe { successFunction.run() }
        }
    }

    @SuppressLint("CheckResult")
    fun onStart() {
        repository.getLastCity()
            .subscribe({
                selectedPlace.postValue(it)
            }, { it.printStackTrace() })
    }

}
