package com.lshershun.weatherapp.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Place(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var lat:Double,
    var lon:Double){
    constructor(lat:Double, lon:Double) : this(0, lat, lon)
}