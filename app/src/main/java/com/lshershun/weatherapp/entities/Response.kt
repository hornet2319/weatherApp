package com.lshershun.weatherapp.entities

import com.google.gson.annotations.Expose

data class Response(
    @Expose val count: Int,
    @Expose val `data`: List<Forecast>
)