package com.lshershun.weatherapp.entities

import android.text.SpannableStringBuilder
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

@Entity
data class Forecast(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var placeId:Int,
    @Expose var app_temp: Double?,
    @Expose var aqi: Double?,
    @Expose var city_name: String?,
    @Expose var clouds: Double?,
    @Expose var country_code: String?,
    @Expose var datetime: String?,
    @Expose var dewpt: Double?,
    @Expose var dhi: Double?,
    @Expose var dni: Double?,
    @Expose var elev_angle: Double?,
    @Expose var ghi: Double?,
    @Expose var h_angle: Int?,
    @Expose var lat: Double?,
    @Expose var lon: Double?,
    @Expose var ob_time: String?,
    @Expose var pod: String?,
    @Expose var pres: Double?,
    @Expose var rh: Double?,
    @Expose var slp: Double?,
    @Expose var solar_rad: Double?,
    @Expose var state_code: String?,
    @Expose var station: String?,
    @Expose var sunrise: String?,
    @Expose var sunset: String?,
    @Expose var temp: Double?,
    @Expose var timezone: String?,
    @Expose var ts: Double?,
    @Expose var uv: Double?,
    @Expose var vis: Double?,
    @Ignore
    @Expose var weather: Weather?,
    @Expose var wind_cdir: String?,
    @Expose var wind_cdir_full: String?,
    @Expose var wind_dir: Double?,
    @Expose var wind_spd: Double?
) {
    constructor(): this(0,0, null,null,null,
        null,null,null,null,null,null,
        null,null,null,null,null,null,null,
        null,null,null,null,null,null,
        null,null,null,null,null,null,null,
        null,null,null,null,null)

}

