package com.lshershun.weatherapp.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

@Entity
data class Weather(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var forecastId:Int,
    @Expose var code: String,
    @Expose var description: String,
    @Expose var icon: String
)