package com.lshershun.weatherapp.repository

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.lshershun.weatherapp.BuildConfig
import com.lshershun.weatherapp.entities.Forecast
import com.lshershun.weatherapp.entities.Place
import com.lshershun.weatherapp.entities.Response
import com.lshershun.weatherapp.tools.ConnectivityChecker
import com.lshershun.weatherapp.tools.rest.Api
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit

import java.util.concurrent.TimeUnit
import kotlin.Exception


class RemoteRepository(private val retrofit: Api, private val connectivityChecker: ConnectivityChecker) {
    fun getForecast(place: Place): Flowable<Forecast> {
        Log.d(BuildConfig.TAG, "get remote forecasts")
        return retrofit.getCurrentWeather(place.lat, place.lon)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { Gson().fromJson(it, Response::class.java) }
            .map { it.data }
            .flatMapIterable { x -> x }
//            .flatMap { Flowable.fromIterable(it.data) }
    }

    fun isInternetConnected(): Boolean {
        return connectivityChecker.isInternetConnected()
    }


}