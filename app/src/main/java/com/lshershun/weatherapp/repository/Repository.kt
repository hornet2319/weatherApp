package com.lshershun.weatherapp.repository

import android.util.Log
import com.google.gson.JsonObject
import com.lshershun.weatherapp.BuildConfig
import com.lshershun.weatherapp.entities.Forecast
import com.lshershun.weatherapp.entities.Place
import com.lshershun.weatherapp.tools.ConnectivityChecker
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface Repository {
    fun getForecasts(place: Place): Flowable<Forecast>
    fun getLastCity(): Single<Place>
    fun savePlace(value: Place): Completable
    fun cleanPlaces(): Completable
    fun isInternetConnected(): Boolean
}

class RepositoryImpl(
    private val localRepository: LocalRepository,
    private val remoteRepository: RemoteRepository
) : Repository {

    override fun isInternetConnected(): Boolean {
        return remoteRepository.isInternetConnected()
    }

    override fun cleanPlaces(): Completable {
        Log.d(BuildConfig.TAG, "cleaning places")
        return localRepository.cleanPlaces()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun savePlace(value: Place): Completable {
        return localRepository.upsertPlace(value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getLastCity(): Single<Place> {
        return localRepository.getLastCity()
            .toSingle()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getForecasts(place: Place): Flowable<Forecast> {
        return localRepository.getForecast(place)
            .mergeWith(
                remoteRepository.getForecast(place)
                    .onExceptionResumeNext { Flowable.empty<Forecast>() }
                    .map { it.placeId = place.id; return@map it }
                    .map { cache(it) }
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun cache(forecast: Forecast): Forecast {
        localRepository.saveForecast(forecast)
        return forecast
    }


}