package com.lshershun.weatherapp.repository

import android.util.Log
import com.lshershun.weatherapp.BuildConfig
import com.lshershun.weatherapp.entities.Forecast
import com.lshershun.weatherapp.entities.Place
import com.lshershun.weatherapp.tools.db.AppDatabase
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers


class LocalRepository(private val db: AppDatabase) {
    fun getLastCity(): Maybe<Place> {
        Log.d(BuildConfig.TAG, "get last place")
        return db.placeDao().getLastPlace()
//        return Maybe.just(Place(-34.0, 151.0)) // test
//        return Maybe.empty();
    }

    fun getForecast(it: Place): Flowable<Forecast> {
        Log.d(BuildConfig.TAG, "get local forecast")
        return db.forecastDao().getForecast(it)

    }

    fun upsertPlace(place: Place): Completable {
        Log.d(BuildConfig.TAG, "save place $place")
        return Completable.create {
            val id = db.placeDao().insert(place)
            if (id.toInt() == -1) {
                db.placeDao().update(place)
            }
            it.onComplete()
        }
    }

    fun saveForecast(forecast: Forecast) {
        Log.d(BuildConfig.TAG, "saving forecast $forecast")
        Completable.create {
            val id = db.forecastDao().insert(forecast)
            if (id.toInt() == -1) {
                db.forecastDao().update(forecast)
            }
            it.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    fun cleanPlaces(): Completable {
        Log.d(BuildConfig.TAG, "clearing places")
        return Completable.create {
            db.placeDao().clear()
            it.onComplete()
        }
    }


}