package com.lshershun.weatherapp.tools.rest

import com.google.gson.JsonObject
import com.lshershun.weatherapp.entities.Forecast
import com.lshershun.weatherapp.entities.Response
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("current")
    fun getCurrentWeather(
        @Query("lat") lat:Double,
        @Query("lon") lon:Double): Flowable<JsonObject>
}