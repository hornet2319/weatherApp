package com.lshershun.weatherapp.tools.di

import android.content.Context
import android.net.ConnectivityManager
import androidx.room.Room
import com.google.gson.GsonBuilder
import com.lshershun.weatherapp.BuildConfig
import com.lshershun.weatherapp.MyApplication
import com.lshershun.weatherapp.repository.LocalRepository
import com.lshershun.weatherapp.repository.RemoteRepository
import com.lshershun.weatherapp.repository.Repository
import com.lshershun.weatherapp.repository.RepositoryImpl
import com.lshershun.weatherapp.tools.ConnectivityChecker
import com.lshershun.weatherapp.tools.db.AppDatabase
import com.lshershun.weatherapp.tools.rest.Api
import com.lshershun.weatherapp.ui.details.ForecastViewModel
import com.lshershun.weatherapp.ui.map.MapsViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private fun getHttpClient(): OkHttpClient {
    val httpClient = OkHttpClient.Builder()
    httpClient.addInterceptor { chain ->
        val original = chain.request()
        val originalHttpUrl = original.url()

        val url = originalHttpUrl.newBuilder()
            .addQueryParameter("key", BuildConfig.API_KEY)
            .build()

        val requestBuilder = original.newBuilder()
            .url(url)

        val request = requestBuilder.build()
        chain.proceed(request)
    }
    return httpClient.build()
}

private val retrofit = retrofit2.Retrofit.Builder()
    .baseUrl(BuildConfig.SERVER_URL)
    .client(getHttpClient())
    .addConverterFactory(GsonConverterFactory.create(getGson()))
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .build()


fun getGson() = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()!!

val appModule = module {
    single { LocalRepository(get()) }
    single { RemoteRepository(get(), get()) }
    factory { ConnectivityChecker(androidContext()) }
    single <Repository> { RepositoryImpl(get(), get()) }
    single<Api> {
        retrofit
            .create(Api::class.java)
    }
    single {
        Room.databaseBuilder(androidContext(), AppDatabase::class.java, "data")
            .fallbackToDestructiveMigration()
            .build()
    }
    viewModel { ForecastViewModel(get()) }
    viewModel { MapsViewModel(get()) }
}


