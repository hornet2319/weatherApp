package com.lshershun.weatherapp.tools.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lshershun.weatherapp.entities.Forecast
import com.lshershun.weatherapp.entities.Place
import com.lshershun.weatherapp.entities.Weather
import com.lshershun.weatherapp.tools.db.dao.ForecastDAO
import com.lshershun.weatherapp.tools.db.dao.PlaceDAO

@Database(entities = [Forecast::class, Place::class, Weather::class], version = 2)
 abstract class AppDatabase : RoomDatabase() {
    abstract fun placeDao() : PlaceDAO
    abstract fun forecastDao() : ForecastDAO
}

