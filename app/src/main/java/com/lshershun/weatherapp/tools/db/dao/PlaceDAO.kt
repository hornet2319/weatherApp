package com.lshershun.weatherapp.tools.db.dao

import androidx.room.*
import com.lshershun.weatherapp.entities.Place
import io.reactivex.Maybe

@Dao
interface PlaceDAO {
    @Query("Select * FROM Place ORDER BY id DESC LIMIT 1")
    fun getLastPlace(): Maybe<Place>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(place: Place) : Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun update(place: Place)

    @Query("DELETE FROM Place")
    fun clear()
}