package com.lshershun.weatherapp.tools.db.dao

import androidx.room.*
import com.lshershun.weatherapp.entities.Forecast
import com.lshershun.weatherapp.entities.Place
import com.lshershun.weatherapp.entities.Weather
import io.reactivex.Flowable

@Dao
abstract class ForecastDAO {
    @Query("Select * FROM Forecast WHERE placeId=:placeId")
    abstract fun getForecast(placeId: Int): Flowable<Forecast>

    @Query("Select * FROM Weather WHERE forecastId=:forecastId")
    abstract fun getWeather(forecastId: Int): Weather

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertForecast(forecast: Forecast): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insertWeather(weather: Weather): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    abstract fun updateForecast(forecast: Forecast)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    abstract fun updateWeather(weather: Weather)

    @Delete
    abstract fun deleteForecast(forecast: Forecast)

    @Delete
    abstract fun deleteWeather(weather: Weather)

    fun delete(forecast: Forecast) {
        forecast.weather?.let {
            deleteWeather(it)
        }
        deleteForecast(forecast)
    }

    fun update(forecast: Forecast) {
        forecast.weather?.let {
            it.forecastId = forecast.id
            updateWeather(it)
        }
        updateForecast(forecast)
    }

    fun insert(forecast: Forecast): Long {
        val id = insertForecast(forecast).toInt()
        forecast.weather?.let {
            if (id != -1) {
                it.forecastId = id
            }
            insertWeather(it)
        }
        return id.toLong()
    }

    fun getForecast(place: Place): Flowable<Forecast> {
        return getForecast(place.id)
            .map { it.weather = getWeather(it.id); return@map it }
    }
}
