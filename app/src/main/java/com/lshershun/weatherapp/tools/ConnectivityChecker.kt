package com.lshershun.weatherapp.tools

import android.content.Context
import android.net.ConnectivityManager

class ConnectivityChecker(private val context: Context) {
    fun isInternetConnected() : Boolean {
        val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (connectivity.activeNetworkInfo != null) {
            connectivity.activeNetworkInfo.isConnected
        } else false
    }
}