package com.lshershun.weatherapp

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.lshershun.weatherapp.tools.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        val context = this
        startKoin {
            modules(appModule)
                .logger(org.koin.android.logger.AndroidLogger())
                .androidContext(context)
        }
    }
}